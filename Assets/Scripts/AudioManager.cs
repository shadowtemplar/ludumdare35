﻿using UnityEngine;
using System.Collections.Generic;

public class AudioManager : MonoSingleton<AudioManager>
{
    public AudioClip[] backgrounds;
    public AudioClip[] randoms;

    public AudioSource randomSound;
    public AudioSource backgroundMusic;

    public float minRandomInterval;
    public float maxRandomInterval;

    private float timeElapsed;
    private float targetTime;

    private int currBackground = 0;

	void Awake () {
        if (minRandomInterval >= maxRandomInterval)
        {
            minRandomInterval = maxRandomInterval - 1;
        }
        targetTime = Random.Range(minRandomInterval, maxRandomInterval);
	}
	
	void Update () {
        if (!backgroundMusic.isPlaying && backgrounds.Length > 0)
        {
            backgroundMusic.PlayOneShot(backgrounds[currBackground]);
            currBackground++;
            if(currBackground > backgrounds.Length -1)
            {
                currBackground = 0;
            }
        }

        if (!randomSound.isPlaying && timeElapsed > targetTime && randoms.Length > 0)
        {
            randomSound.PlayOneShot(randoms[Random.Range(0,randoms.Length)]);
            targetTime = Random.Range(minRandomInterval, maxRandomInterval);
        } else
        {
            timeElapsed += Time.deltaTime;
            if (timeElapsed > maxRandomInterval)
            {
                timeElapsed = maxRandomInterval;
            }
        }
    }
}
