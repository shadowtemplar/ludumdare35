﻿using UnityEngine;
using System.Collections;

public class AudioQueue<AudioClip> : ArrayList{

    private int index;

    public AudioQueue()
    {
        index = 0;
    }

    public AudioClip Next()
    {
        AudioClip clip;
        if (this.Count>0)
        {
             if(index>=this.Count) {
                Reset();
             }
            clip = (AudioClip)this[index];
            //this.Remove(clip);
            index++;
            return clip ;
        }
        return default(AudioClip);
    }  

    public void Reset()
    {
        index = 0;
    }

	
	
}
