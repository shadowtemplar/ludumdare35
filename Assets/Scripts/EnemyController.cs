﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class EnemyController : MonoBehaviour {

    private bool lightOn = false;
    public Transform player;
    public float enemySpeedDark = 2f;
    public float enemySpeedBright = 5f;

    NavMeshAgent agent;

    bool dead = false;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("FirstPersonCharacter").transform;
    }

	public bool LightOn
    {
        get
        {
            return lightOn;
        }
        set
        {
            lightOn = value;
            if (!value) {
                agent.speed = enemySpeedDark;
            }
            else
            {
                agent.speed = enemySpeedBright;
            }
        }
    }
    
    void Update()
    {
        agent.SetDestination(player.position);
        if(Vector3.Distance(gameObject.transform.position, player.position) < 2f)
        {
            if(!dead)
            {
                dead = true;
                GameManager.instance.spawned = false;
                SceneManager.LoadScene("Game Over 2", LoadSceneMode.Single);
            }
        }
    }


}
