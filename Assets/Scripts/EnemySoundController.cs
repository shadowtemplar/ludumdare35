﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class EnemySoundController : MonoBehaviour {

    [Range(1f,30f)]
    public float maxSoundDist = 15f;

    public Transform player;

    AudioSource soundPlayer;

    void Start() {
        soundPlayer = GetComponent<AudioSource>();
    }

    void Update() {
        if(player != null)
        {
            float dist = Vector3.Distance(transform.position, player.position);
            Debug.Log(dist);
            if(dist > maxSoundDist)
            {
                soundPlayer.volume = 0f;
            } else
            {
                soundPlayer.volume = 1 - (dist / maxSoundDist);
            }
        }
    }
}
