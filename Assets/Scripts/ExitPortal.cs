﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public class ExitPortal : MonoBehaviour {

    public FirstPersonController player;
    public GameObject enemy;
    public LanternController lantern;

    bool gameEnded = false;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player" && !gameEnded)
        {
            SceneManager.LoadScene("WinState", LoadSceneMode.Additive);
            gameEnded = true;
            player.enabled = false;
            enemy.SetActive(false);
            lantern.enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    void Update() {
        if(enemy == null)
        {
            var tmp = GameObject.Find("EnemyDad(Clone)");
            if(tmp != null)
            {
                enemy = tmp;
            }
        }
    }
}
