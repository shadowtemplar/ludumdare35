﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoSingleton<GameManager> {

    public GameObject player;
    public Transform particles;
    public GameObject enemy;
    public bool spawned = false;

    private float timeSinceStart;

    private bool lanternOn = true;

	// Use this for initialization
	void Start()
    {

    }
	
	// Update is called once per frame
	void Update () {
        if (Vector3.Distance(player.transform.position, new Vector3(0, 0, 0)) > 4f && spawned == false)
        {
            spawned = true;
            StartCoroutine(SpawnEnemy());

        }
	}

    private IEnumerator SpawnEnemy()
    {
        var prt = Instantiate(particles);
        yield return new WaitForSeconds(5f);
        Instantiate(enemy);
        Destroy(prt.gameObject);
    }
   
}
