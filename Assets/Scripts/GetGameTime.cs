﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

[RequireComponent(typeof(Text))]
public class GetGameTime : MonoBehaviour {

	void Start () {
        Text uit = GetComponent<Text>();
        uit.text = uit.text + Math.Round(TimeManager.getEndTime(), 1).ToString() + " seconds";
    }
}
