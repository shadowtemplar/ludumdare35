﻿using UnityEngine;
using System.Collections;

public class Grid : MonoSingleton<Grid> {

    public float cellSize = 4.8768f;
    public int gridXSize = WorldGenerator.instance.columnNo;
    public int gridYSize = WorldGenerator.instance.rowNo;

    public ArrayList Cells = new ArrayList();

   

    public struct Cell
    {
        public WorldGenerator.Room room;
        public int roomRow;
        public int roomColumn;
        public Vector3 cellCenterPos;
        public Vector3 southSidePos;
        public Vector3 northSidePos;
        public Vector3 eastSidePos;
        public Vector3 westSidePos;

        public void setParameters(WorldGenerator.Room r, Vector3 cellCenter, Vector3 east, Vector3 south, Vector3 north, Vector3 west, int row, int column)
        {
            roomRow = row;
            roomColumn = column;
            room = r;
            cellCenterPos = cellCenter;
            eastSidePos = east;
            southSidePos = south;
            northSidePos = north;
            westSidePos = west;
        }
    } 

    public void GenerateGrid()
    {
        if (Cells.Count != 0)
        {
            Cells.Clear();
        }
        for(int r = 0; r<WorldGenerator.instance.rowNo; r++)
        {
            for(int c = 0; c<WorldGenerator.instance.columnNo; c++)
            {
                Vector3 center = new Vector3((c * Grid.instance.cellSize), 0, -(r * Grid.instance.cellSize));
                Vector3 east = new Vector3((center.x + (Grid.instance.cellSize * 0.5f)), center.y, center.z);
                Vector3 west = new Vector3((center.x - (Grid.instance.cellSize * 0.5f)), center.y, center.z);
                Vector3 south = new Vector3(center.x, center.y, (center.z - (Grid.instance.cellSize * 0.5f)));
                Vector3 north = new Vector3(center.x, center.y, (center.z + (Grid.instance.cellSize * 0.5f)));
                Cell cell = new Cell();
                cell.setParameters(WorldGenerator.instance.roomMatrix[r, c], center, east, south, north, west, r, c);
                Cells.Add(cell);
                
            }
        }
    }
}
