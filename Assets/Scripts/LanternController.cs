﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class LanternController : MonoBehaviour {

    private Animator anim;
    private bool _isLit = true;

    public EnemyController enemy;

    public bool isLit
    {
        get
        {
            return _isLit;
        }
        set
        {
            _isLit = value;
            anim.SetBool("IsLit", value);
            if (enemy != null)
            {
                enemy.LightOn = value;
            }
        }
    }

    void Regenrate()
    {
        RoomsSpawn.instance.RegenerateWorld();
    }

	void Start () {
        anim = GetComponent<Animator>();
        TimeManager.setStartTime();
	}
	
	void Update () {
        if(enemy == null)
        {
            var tmp = GameObject.Find("EnemyDad(Clone)");
            if(tmp != null)
            {
                enemy = tmp.GetComponent<EnemyController>();
                enemy.LightOn = isLit;
            }
        }
	    if(Input.GetMouseButtonDown(0))
        {
            isLit = !isLit;
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
	}
}
