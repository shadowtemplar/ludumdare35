﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuController : MonoBehaviour {

    public string LevelName;

    AsyncOperation sceneLoad;

    public void StartGame()
    {
        StartCoroutine(LoadMain()); //Temporary, will start animation which will trigger SwitchLevel()
    }

    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName, LoadSceneMode.Single);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public IEnumerator LoadMain()
    {
        sceneLoad = SceneManager.LoadSceneAsync(LevelName);
        yield return sceneLoad;
    }
}
