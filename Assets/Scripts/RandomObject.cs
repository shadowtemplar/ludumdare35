﻿using UnityEngine;
using System.Collections;

public class RandomObject : MonoBehaviour {

    public GameObject[] availableObjects;
    public bool copyRotation = false;

    void Start()
    {
        int prefabIndex = Random.Range(0, availableObjects.Length);
        GameObject prefab = availableObjects[prefabIndex];
        if (prefab == null) return;
        GameObject subsitute = (GameObject)Instantiate(prefab, gameObject.transform.position, (copyRotation ? gameObject.transform.rotation : prefab.transform.rotation)) as GameObject;
        subsitute.transform.parent = gameObject.transform;
    }
}
