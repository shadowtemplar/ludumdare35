﻿using UnityEngine;
using System.Collections;

public class ReleaseCursor : MonoBehaviour {

	void Start () {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
}
