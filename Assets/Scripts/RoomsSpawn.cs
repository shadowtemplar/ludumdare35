﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomsSpawn : MonoSingleton<RoomsSpawn> {

    public GameObject wall;
    public GameObject floor;
    public GameObject ceiling;
    public int width = 5;
    public int height = 5;
    public GameObject player;
    GameObject parent;

    bool firstGeneration = true;

    void Start()
    {
        parent = new GameObject();
        WorldGenerator.instance.columnNo = width;
        WorldGenerator.instance.rowNo = height;
        WorldGenerator.instance.generateWorldV2();
        Grid.instance.GenerateGrid();
        GenerateCelingFloor();
        SpawnRooms();
       
    }

    public void RegenerateWorld()
    {
        //Vector3 playerPos = player.transform.position;
        int playerRow = Mathf.FloorToInt((-(player.transform.position.z) + Grid.instance.cellSize*0.5f )/ Grid.instance.cellSize);
        int playerColumn = Mathf.FloorToInt(((player.transform.position.x) + Grid.instance.cellSize*0.5f)/ Grid.instance.cellSize);

        if (playerRow < 0) playerRow = 0;
        if (playerColumn < 0) playerColumn = 0;

        var children = new List<GameObject>();
        foreach (Transform child in parent.transform) children.Add(child.gameObject);
        foreach(GameObject c in children)
        {
            if(c.name != playerRow + "," + playerColumn)
            {
                Destroy(c);
            }
        }
        //children.ForEach(child => Destroy(child));
        
        WorldGenerator.instance.generateWorldV2();
        Grid.instance.GenerateGrid();
        SpawnRooms();
    }


    public void GenerateCelingFloor()
    {
        foreach (Grid.Cell c in Grid.instance.Cells)
        {
            GameObject flr = Instantiate(floor) as GameObject;
            flr.transform.position = c.cellCenterPos;
            //flr.hideFlags = HideFlags.HideInHierarchy;

            GameObject clg = Instantiate(ceiling) as GameObject;
            clg.transform.position = c.cellCenterPos + (Vector3.up * 4.25f);
        }

    }
    public void SpawnRooms()
    {
        foreach (Grid.Cell c in Grid.instance.Cells)
        {
            int playerRow = Mathf.FloorToInt((-(player.transform.position.z) + Grid.instance.cellSize * 0.5f) / Grid.instance.cellSize);
            int playerColumn = Mathf.FloorToInt(((player.transform.position.x) + Grid.instance.cellSize * 0.5f) / Grid.instance.cellSize);
            if (c.roomRow == playerRow && c.roomColumn == playerColumn && !firstGeneration) continue;

            firstGeneration = false;

            GameObject parentCell = new GameObject(c.roomRow+","+c.roomColumn);
            parentCell.transform.position = c.cellCenterPos;
            parentCell.transform.SetParent(parent.transform);

            GameObject flr = Instantiate(floor) as GameObject;
            flr.transform.SetParent(parentCell.transform);
            flr.transform.position = c.cellCenterPos;
            //flr.hideFlags = HideFlags.HideInHierarchy;

            GameObject clg = Instantiate(ceiling) as GameObject;
            clg.transform.SetParent(parentCell.transform);
            clg.transform.position = c.cellCenterPos + (Vector3.up * 4.25f);
            //clg.hideFlags = HideFlags.HideInHierarchy;

            if (c.room.getWallType(WorldGenerator.roomDirections.E) == WorldGenerator.wallType.wall)
            {
                GameObject newWall = Instantiate(wall);
                newWall.transform.SetParent(parentCell.transform);
                newWall.hideFlags = HideFlags.HideInHierarchy;
                newWall.transform.position = c.eastSidePos;
                newWall.transform.eulerAngles = new Vector3(-90, 0, 0);
                
            }
            
            
            if(c.room.getWallType(WorldGenerator.roomDirections.W) == WorldGenerator.wallType.wall)
            {
                GameObject newWall = Instantiate(wall);
                newWall.transform.SetParent(parentCell.transform);
                newWall.hideFlags = HideFlags.HideInHierarchy;
                newWall.transform.position = c.westSidePos;
                newWall.transform.eulerAngles = new Vector3(-90, 180, 0);

            }
            
            
            if (c.room.getWallType(WorldGenerator.roomDirections.S) == WorldGenerator.wallType.wall)
            {
                GameObject newWall = Instantiate(wall);
                newWall.transform.SetParent(parentCell.transform);
                newWall.hideFlags = HideFlags.HideInHierarchy;
                newWall.transform.position = c.southSidePos;
                newWall.transform.eulerAngles = new Vector3(-90, 90, 0);

            }


            if (c.room.getWallType(WorldGenerator.roomDirections.N) == WorldGenerator.wallType.wall)
            {
                GameObject newWall = Instantiate(wall);
                newWall.transform.SetParent(parentCell.transform);
                newWall.hideFlags = HideFlags.HideInHierarchy;
                newWall.transform.position = c.northSidePos;
                newWall.transform.eulerAngles = new Vector3(-90, -90, 0);
            }
        }
    }




}
