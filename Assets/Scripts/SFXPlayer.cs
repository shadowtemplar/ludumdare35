﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SFXPlayer : MonoBehaviour {

    private AudioSource sfx;

	void Start () {
        sfx = GetComponent<AudioSource>();
	}

    public void PlaySound()
    {
        sfx.PlayOneShot(sfx.clip);
    }	
}
