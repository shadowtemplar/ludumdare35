﻿using UnityEngine;

public static class TimeManager{


    static float startTime;
	// Update is called once per frame
	public static void setStartTime()
    {
        startTime = Time.time;
    }

    public static float getEndTime()
    {
        return Time.time - startTime;
    }
}
