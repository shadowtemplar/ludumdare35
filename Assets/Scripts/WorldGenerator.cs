﻿using System;
using System.Collections;

public class WorldGenerator: MonoSingleton<WorldGenerator>
{

    //grid size
    public int columnNo = 20;
    public int rowNo = 20;

    //room generation
    private int maxNoWallsPerRoom = 3;
    private int wallProbability = 70; // out of 100 between wall and connection (empty/door)
    private int doorProbability = 0; // out of 100 between door and empty
    private int maxRoomTriesUntilRegenerate = 50;

    private bool isRegenerating = false;

    //player and goal position
    Tile playerPos;
    Tile goalPos;

    public enum wallType { empty, door, wall };
    public enum roomDirections { N, E, S, W };
    public Room[,] roomMatrix;
    Random rand;

    private struct Tile
    {
        //row and column that the tile is on
        public int row { get; set; }
        public int column { get; set; }

        public Tile(int row, int column)
        {
            this.row = row;
            this.column = column;
        }
    }

    public class Room
    {
        //type of walls for the room - indexes represent the direction it is in from the room
        public int[] roomWalls;
        //array of 0 as 1 (acting as bools) to specify if the wall in a certain direction was already fixed
        //we do not want to change previously set walls
        public int[] fixedRoomWalls;

        public Room()
        {
            roomWalls = new int[Enum.GetNames(typeof(roomDirections)).Length];
            fixedRoomWalls = new int[Enum.GetNames(typeof(roomDirections)).Length];
        }

        public Room(Room r)
        {
            roomWalls = r.roomWalls;
            fixedRoomWalls = r.fixedRoomWalls;
        }

        public bool setWall(roomDirections wall, wallType wallType)
        {
            //if the wall was already set (is fixed) then do not change it
            if (fixedRoomWalls[(int)wall] == 1)
            {
                return false;
            }
            else
            {
                //set wall type and fix the wall so it can't be changed
                roomWalls[(int)wall] = (int)wallType;
                fixedRoomWalls[(int)wall] = 1;
                return true;
            }
        }

        //return the wall type in a certain direction
        public wallType getWallType(roomDirections direction)
        {
            return (wallType)roomWalls[(int)direction];
        }

        public void fixWalls()
        {
            for (int i = 0; i < fixedRoomWalls.Length; i++)
            {
                fixedRoomWalls[i] = 1;
            }
        }
    }


    public void init()
    {
        //init the room matrix
        roomMatrix = new Room[rowNo, columnNo];
        for (int i = 0; i < rowNo; i++)
        {
            for (int j = 0; j < columnNo; j++)
            {
                roomMatrix[i, j] = new Room();
            }
        }
        rand = new Random();

        //hardcoded player and goal positions
        playerPos = new Tile(0, 0);
        goalPos = new Tile(rowNo - 1, columnNo - 1);
    }

    public void generateWorld()
    {
        //first generate the edges of the map
        isRegenerating = false;
        generateEdges();
        generateRooms();
    }

    public void generateWorldV2()
    {
        init();
        generateEdges();
        addInnerWalls(true, 0, rowNo - 1, 0, columnNo - 1);
    }

    //restart generation from scrap - call if generation fails for a reason
    private void restartGeneration()
    {
        init();
        generateEdges();
        generateRooms();
    }

    //regenerate rooms, but keep the room that the player is in the same
    public void regenerateRooms(int playerRow, int playerColumn)
    {
        isRegenerating = true;
        Room playerRoom = new Room(roomMatrix[playerRow, playerColumn]);
        init();
        //store player room setup
        roomMatrix[playerRow, playerColumn] = new Room(playerRoom);
        roomMatrix[playerRow, playerColumn].fixWalls();
        generateEdges();
        foreach (roomDirections direction in Enum.GetValues(typeof(roomDirections)))
        {
            if (getConnectingDirection(direction) >= 0)
            {
                Room nextRoom = getRoomInDirectionFrom(direction, playerRow, playerColumn);
                if (nextRoom != null)
                    nextRoom.setWall((roomDirections)getConnectingDirection(direction), roomMatrix[playerRow, playerColumn].getWallType(direction));
            }
        }
        playerPos.row = playerRow;
        playerPos.column = playerColumn;
        generateRooms();
    }

    public void addInnerWalls(bool horizontal, int minRow, int maxRow, int minColumn, int maxColumn)
    {
        if (horizontal)
        {
            if (maxColumn - minColumn < 1)
            {
                return;
            }

            var y = rand.Next(minRow, maxRow);
            addHWall(minColumn, maxColumn, y);

            addInnerWalls(!horizontal, minRow, y, minColumn, maxColumn);
            addInnerWalls(!horizontal, y + 1, maxRow, minColumn, maxColumn);
        }
        else {
            if (maxRow - minRow < 1)
            {
                return;
            }

            var x = rand.Next(minColumn, maxColumn);
            addVWall(minRow, maxRow, x);

            addInnerWalls(!horizontal, minRow, maxRow, minColumn, x);
            addInnerWalls(!horizontal, minRow, maxRow, x + 1, maxColumn);
        }
    }

    private void addHWall(int minColumn, int maxColumn, int row)
    {
        var hole = rand.Next(minColumn, maxColumn + 1);

        for (var i = minColumn; i <= maxColumn; i++)
        {
            if (i == hole) setRoomWall(row, i, roomDirections.E, wallType.empty);
            else setRoomWall(row, i, roomDirections.E, wallType.wall);
        }
    }

    private void setRoomWall(int row, int column, roomDirections dir, wallType type)
    {
        roomMatrix[row, column].setWall(dir, type);
        Room temp = getRoomInDirectionFrom(dir, row, column);
        if (temp != null)
        {
            temp.setWall((roomDirections)getConnectingDirection(dir), type);
        }
    }

    private void addVWall(int minRow, int maxRow, int column)
    {
        var hole = rand.Next(minRow, maxRow + 1);

        for (var i = minRow; i <= maxRow; i++)
        {
            if (i == hole) setRoomWall(i, column, roomDirections.S, wallType.empty);
            else setRoomWall(i, column, roomDirections.S, wallType.wall);
        }
    }

    public void generateRooms()
    {
        //remaining rooms to generate
        Queue remainingRooms = new Queue();
        //rooms already generated
        bool[,] generatedRooms = new bool[rowNo, columnNo];
        Tile currRoom = new Tile(playerPos.row, playerPos.column);
        //if was unable to generate the room, retsart the generation process
        if (!generateRoom(currRoom.row, currRoom.column))
        {
            if (!isRegenerating)
            {
                restartGeneration();
            }
            else
            {
                regenerateRooms(playerPos.row, playerPos.column);
            }
        }
        //add connecting rooms to the Queue of remaining rooms so that we know we have to generate them
        foreach (roomDirections direction in Enum.GetValues(typeof(roomDirections)))
        {
            //check if there is a connection the that direction
            wallType currWallType = roomMatrix[currRoom.row, currRoom.column].getWallType(direction);
            if (currWallType == wallType.door || currWallType == wallType.empty)
            {
                //only add to queue if there is a room in that direction and if it haas not been already generated
                Tile? nextRoom = getRoomCoordsInDirectionFrom(direction, currRoom.row, currRoom.column);
                if (nextRoom != null && !generatedRooms[((Tile)nextRoom).row, ((Tile)nextRoom).column])
                    remainingRooms.Enqueue(nextRoom);
            }
        }
        //remember that we generated the room
        generatedRooms[currRoom.row, currRoom.column] = true;
        //go through rooms in the queue and generate them
        while (remainingRooms.Count > 0)
        {
            //check is done because sometimes duplicates are in the Queue and later duplicates could still be in the queue after first one was already generated
            do
            {
                currRoom = (Tile)remainingRooms.Dequeue();
            }
            while (generatedRooms[((Tile)currRoom).row, ((Tile)currRoom).column] && remainingRooms.Count > 0);
            //do only if it found an ungenerated room
            if (!generatedRooms[((Tile)currRoom).row, ((Tile)currRoom).column])
            {
                //same as above for the first room
                if (!generateRoom(currRoom.row, currRoom.column))
                {
                    if (!isRegenerating)
                    {
                        restartGeneration();
                    }
                    else
                    {
                        regenerateRooms(playerPos.row, playerPos.column);
                    }
                }
                foreach (roomDirections direction in Enum.GetValues(typeof(roomDirections)))
                {
                    wallType currWallType = roomMatrix[currRoom.row, currRoom.column].getWallType(direction);
                    if (currWallType == wallType.door || currWallType == wallType.empty)
                    {
                        Tile? nextRoom = getRoomCoordsInDirectionFrom(direction, currRoom.row, currRoom.column);
                        if (nextRoom != null && !generatedRooms[((Tile)nextRoom).row, ((Tile)nextRoom).column])
                            remainingRooms.Enqueue(nextRoom);
                    }
                }
                generatedRooms[currRoom.row, currRoom.column] = true;
            }
        }

    }

    //generate walls on the edges of the tile map
    private void generateEdges()
    {
        for (int i = 0; i < columnNo; i++)
        {
            roomMatrix[0, i].setWall(roomDirections.N, wallType.wall);
            roomMatrix[rowNo - 1, i].setWall(roomDirections.S, wallType.wall);
        }
        for (int i = 0; i < rowNo; i++)
        {
            roomMatrix[i, 0].setWall(roomDirections.W, wallType.wall);
            roomMatrix[i, columnNo - 1].setWall(roomDirections.E, wallType.wall);
        }
    }

    //generate room at a certain row and column - return true if could be generated and false if was unable to
    private bool generateRoom(int row, int column)
    {
        //keep track of the original room fixed walls
        Room initRoom = new Room(roomMatrix[row, column]);
        generateRoomWallsRandom(row, column);
        //store how many times we tried to generate the room unsuccessfully
        int roomTries = 0;
        //while room is not valid, generate random room again
        while (!checkRoomValidity(row, column))
        {
            //if we failed to generate a valid room for too many tries - return false
            //this is done because there may not be a vaid configuration - se we want to restart the generation process since something probably went wrong
            if (roomTries++ > maxRoomTriesUntilRegenerate)
            {
                return false;
            }
            //reset walls (important for fixed walls so that the rooma ctually changes again)
            roomMatrix[row, column] = new Room(initRoom);
            generateRoomWallsRandom(row, column);
        }
        //set the connecting walls for each adjecent room to the corresponding ones that we created in the room
        foreach (roomDirections direction in Enum.GetValues(typeof(roomDirections)))
        {
            if (getConnectingDirection(direction) >= 0)
            {
                Room nextRoom = getRoomInDirectionFrom(direction, row, column);
                if (nextRoom != null)
                    nextRoom.setWall((roomDirections)getConnectingDirection(direction), roomMatrix[row, column].getWallType(direction));
            }
        }
        return true;
    }

    //generate a random room configuration at a certain row and column
    private void generateRoomWallsRandom(int row, int column)
    {
        //keep track of how many walls (thata re not connections) are in the room
        int currWallNo = 0;
        //check for previously fixed walls
        foreach (roomDirections direction in Enum.GetValues(typeof(roomDirections)))
        {
            if (roomMatrix[row, column].getWallType(direction) == wallType.wall)
            {
                currWallNo++;
            }
        }
        //for each direction that the room can have walls in, generate random wals or connections based on probabilities
        foreach (roomDirections direction in Enum.GetValues(typeof(roomDirections)))
        {
            if (rand.Next(1, 100) <= wallProbability && currWallNo < maxNoWallsPerRoom)
            {
                roomMatrix[row, column].setWall(direction, wallType.wall);
                currWallNo++;
            }
            else
            {
                if (rand.Next(1, 100) <= doorProbability)
                {
                    roomMatrix[row, column].setWall(direction, wallType.door);
                }
                else
                {
                    roomMatrix[row, column].setWall(direction, wallType.empty);
                }
            }
        }
    }

    //check if a room is valid by making sure that from the room you can reach the goal
    //similar to how generation works
    private bool checkRoomValidity(int row, int column)
    {
        //queue of remaining rooms to check for goal
        Queue remainingRooms = new Queue();
        //store already visited rooms
        bool[,] visitedRooms = new bool[rowNo, columnNo];
        Tile currRoom = new Tile(row, column);
        //if the vurrent room is where the goal is, return true
        if (currRoom.row == goalPos.row && currRoom.column == goalPos.row)
        {
            return true;
        }
        //get all connecting rooms and add tham to the remainingRooms Queue to check for goal location
        foreach (roomDirections direction in Enum.GetValues(typeof(roomDirections)))
        {
            wallType currWallType = roomMatrix[currRoom.row, currRoom.column].getWallType(direction);
            if (currWallType == wallType.door || currWallType == wallType.empty)
            {
                Tile? nextRoom = getRoomCoordsInDirectionFrom(direction, currRoom.row, currRoom.column);
                if (nextRoom != null && !visitedRooms[((Tile)nextRoom).row, ((Tile)nextRoom).column])
                    remainingRooms.Enqueue(nextRoom);
            }
        }
        //remember that you visited the room
        visitedRooms[currRoom.row, currRoom.column] = true;
        while (remainingRooms.Count > 0)
        {
            //check is done because sometimes duplicates are in the Queue and later duplicates could still be in the queue after first one was already visited
            do
            {
                currRoom = (Tile)remainingRooms.Dequeue();
            }
            while (visitedRooms[((Tile)currRoom).row, ((Tile)currRoom).column] && remainingRooms.Count > 0);
            //same as above
            if (!visitedRooms[((Tile)currRoom).row, ((Tile)currRoom).column])
            {
                if (!(currRoom.row == goalPos.row && currRoom.column == goalPos.row))
                {

                    foreach (roomDirections direction in Enum.GetValues(typeof(roomDirections)))
                    {
                        wallType currWallType = roomMatrix[currRoom.row, currRoom.column].getWallType(direction);
                        if (currWallType == wallType.door || currWallType == wallType.empty)
                        {
                            Tile? nextRoom = getRoomCoordsInDirectionFrom(direction, currRoom.row, currRoom.column);
                            if (nextRoom != null && !visitedRooms[((Tile)nextRoom).row, ((Tile)nextRoom).column])
                                remainingRooms.Enqueue(nextRoom);
                        }
                    }
                    visitedRooms[currRoom.row, currRoom.column] = true;
                }
                else
                {
                    return true;
                }
            }

        }
        //return false if goal was not found
        return false;
    }

    //get the adjecent Tile in a certain direction from a room at position row and column
    private Tile? getRoomCoordsInDirectionFrom(roomDirections direction, int row, int column)
    {
        Tile? t = null;
        switch (direction)
        {
            case roomDirections.N: if (row > 0) t = new Tile(row - 1, column); break;
            case roomDirections.S: if (row < rowNo - 1) t = new Tile(row + 1, column); break;
            case roomDirections.W: if (column > 0) t = new Tile(row, column - 1); break;
            case roomDirections.E: if (column < columnNo - 1) t = new Tile(row, column + 1); break;
            default: break;
        }
        return t;
    }

    //get the adjecent Room in a certain direction from a room at position row and column
    private Room getRoomInDirectionFrom(roomDirections direction, int row, int column)
    {
        Tile? coords = getRoomCoordsInDirectionFrom(direction, row, column);
        if (coords != null)
        {
            return roomMatrix[((Tile)coords).row, ((Tile)coords).column];
        }
        return null;
    }

    //get the opposite direction (as an int) to a room direction
    //this would be the corresponding direction to a current room from a room in the given direction
    //so if room1 connects to room2 in direction N, then room2 connects to room1 in direction S
    private int getConnectingDirection(roomDirections direction)
    {
        int result = -1;
        switch (direction)
        {
            case roomDirections.N: result = (int)roomDirections.S; break;
            case roomDirections.S: result = (int)roomDirections.N; break;
            case roomDirections.W: result = (int)roomDirections.E; break;
            case roomDirections.E: result = (int)roomDirections.W; break;
            default: break;
        }
        return result;
    }

}

